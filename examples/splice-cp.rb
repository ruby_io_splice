#!/usr/bin/env ruby
# -*- encoding: binary -*-
# This example is no longer valid, IO.copy_stream is faster in Ruby 2.2+
# since it uses sendfile directly, which now allows direct file-to-file
# copying (on Linux only) with fewer syscalls than splice.
