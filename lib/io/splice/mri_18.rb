# :enddoc:
require "io/nonblock"
module IO::Splice
  class << self
    remove_method :full
    remove_method :partial
  end
  def self.full(src, dst, len, src_offset)
    dst.to_io.nonblock = src.to_io.nonblock = true
    spliced = 0
    case n = IO.trysplice(src, src_offset, dst, nil, len, IO::Splice::F_MOVE)
    when :EAGAIN
      src.to_io.wait
      IO.select(nil, [dst])
    when Integer
      spliced += n
      len -= n
      src_offset += n if src_offset
    when nil
      break
    end while len > 0
    spliced
  end

  def self.partial(src, dst, len, src_offset)
    dst.to_io.nonblock = src.to_io.nonblock = true
    begin
      src.to_io.wait
      IO.select(nil, [dst])
      rv = IO.trysplice(src, src_offset, dst, nil, len, IO::Splice::F_MOVE)
    end while rv == :EAGAIN
    rv
  end
end
