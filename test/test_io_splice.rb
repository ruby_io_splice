# -*- encoding: binary -*-
require 'test/unit'
require 'tempfile'
require 'socket'
require 'io/nonblock'
require 'timeout'
$-w = true
require 'io/splice'

class Test_IO_Splice < Test::Unit::TestCase

  def self.mri?
    (defined?(RUBY_ENGINE) && RUBY_ENGINE == "ruby") || !defined?(RUBY_ENGINE)
  end

  def test_splice
    str = 'abcde'
    size = 5
    rd, wr = IO.pipe
    tmp = Tempfile.new('ruby_io_splice')

    assert_nothing_raised {
      tmp.syswrite(str)
      tmp.sysseek(0)
    }

    nr = IO.splice(tmp.fileno, nil, wr.fileno, nil, size, 0)
    assert_equal size, nr
    assert_equal str, rd.sysread(size)
  end

  def test_splice_io
    str = 'abcde'
    size = 5
    rd, wr = IO.pipe
    tmp = Tempfile.new('ruby_io_splice')

    assert_nothing_raised {
      tmp.syswrite(str)
      tmp.sysseek(0)
    }

    nr = IO.splice(tmp, nil, wr, nil, size, 0)
    assert_equal size, nr
    assert_equal str, rd.sysread(size)
  end

  def test_splice_io_noflags
    str = 'abcde'
    size = 5
    rd, wr = IO.pipe
    tmp = Tempfile.new('ruby_io_splice')

    assert_nothing_raised {
      tmp.syswrite(str)
      tmp.sysseek(0)
    }

    nr = IO.splice(tmp, nil, wr, nil, size)
    assert_equal size, nr
    assert_equal str, rd.sysread(size)
  end

  def test_trysplice_io_noflags
    str = 'abcde'
    size = 5
    rd, wr = IO.pipe
    tmp = Tempfile.new('ruby_io_splice')

    assert_nothing_raised {
      tmp.syswrite(str)
      tmp.sysseek(0)
    }

    nr = IO.trysplice(tmp, nil, wr, nil, size)
    assert_equal size, nr
    assert_equal str, rd.sysread(size)
  end

  def test_splice_io_ish
    str = 'abcde'
    size = 5
    rd, wr = IO.pipe
    tmp = Tempfile.new('ruby_io_splice')
    io_ish = [ tmp ]
    def io_ish.to_io
      first.to_io
    end

    assert_nothing_raised {
      tmp.syswrite(str)
      tmp.sysseek(0)
    }

    nr = IO.splice(io_ish, nil, wr, nil, size, 0)
    assert_equal size, nr
    assert_equal str, rd.sysread(size)
  end

  def test_splice_in_offset
    str = 'abcde'
    off = 3
    len = 2
    rd, wr = IO.pipe
    tmp = Tempfile.new('ruby_io_splice')

    assert_nothing_raised {
      tmp.syswrite(str)
      tmp.sysseek(0)
    }

    nr = IO.splice(tmp.fileno, off, wr.fileno, nil, len, 0)
    assert_equal len, nr
    assert_equal 'de', rd.sysread(len)
  end

  def test_splice_out_offset
    str = 'abcde'
    rd, wr = IO.pipe
    tmp = Tempfile.new('ruby_io_splice')

    assert_nothing_raised { wr.syswrite(str) }
    nr = IO.splice(rd.fileno, nil, tmp.fileno, 3, str.size, 0)
    assert_equal 5, nr
    assert_nothing_raised { tmp.sysseek(0) }
    assert_equal "\0\0\0abcde", tmp.sysread(9)
  end

  def test_splice_nonblock
    rd, wr = IO.pipe
    tmp = Tempfile.new('ruby_io_splice')

    assert_raises(Errno::EAGAIN) {
      IO.splice(rd.fileno, nil, tmp.fileno, 0, 5, IO::Splice::F_NONBLOCK)
    }
  end

  def test_trysplice_nonblock
    rd, wr = IO.pipe
    tmp = Tempfile.new('ruby_io_splice')
    assert_equal :EAGAIN,
           IO.trysplice(rd, nil, tmp, 0, 5, IO::Splice::F_NONBLOCK)
  end

  def test_trysplice_nonblock_noargs
    rd, wr = IO.pipe
    tmp = Tempfile.new('ruby_io_splice')
    assert_equal :EAGAIN, IO.trysplice(rd, nil, tmp, 0, 5)
    assert_equal :EAGAIN, IO.trysplice(rd, nil, tmp, 0, 5, IO::Splice::F_MORE)
  end

  def test_splice_eof
    rd, wr = IO.pipe
    tmp = Tempfile.new('ruby_io_splice')
    wr.syswrite 'abc'
    wr.close

    nr = IO.splice(rd.fileno, nil, tmp.fileno, 0, 5, IO::Splice::F_NONBLOCK)
    assert_equal 3, nr
    assert_raises(EOFError) {
      IO.splice(rd.fileno, nil, tmp.fileno, 0, 5, IO::Splice::F_NONBLOCK)
    }
  end

  def test_trysplice_eof
    rd, wr = IO.pipe
    tmp = Tempfile.new('ruby_io_splice')
    wr.syswrite 'abc'
    wr.close

    nr = IO.trysplice(rd, nil, tmp, 0, 5, IO::Splice::F_NONBLOCK)
    assert_equal 3, nr
    assert_nil IO.trysplice(rd, nil, tmp, 0, 5, IO::Splice::F_NONBLOCK)
  end

  def test_splice_nonblock_socket
    server = TCPServer.new('127.0.0.1', 0)
    port = server.addr[1]
    rp, wp = IO.pipe
    rs = TCPSocket.new('127.0.0.1', port)
    rs.nonblock = true
    assert_raises(Errno::EAGAIN) { IO.splice(rs, nil, wp, nil, 1024, 0) }
    rs.close
    server.close
  end

  def test_tee
    str = 'abcde'
    size = 5
    rda, wra = IO.pipe
    rdb, wrb = IO.pipe

    assert_nothing_raised { wra.syswrite(str) }
    nr = IO.tee(rda.fileno, wrb.fileno, size, 0)
    assert_equal 5, nr
    assert_equal str, rdb.sysread(5)
    assert_equal str, rda.sysread(5)
  end

  def test_trytee
    str = 'abcde'
    size = 5
    rda, wra = IO.pipe
    rdb, wrb = IO.pipe

    assert_nothing_raised { wra.syswrite(str) }
    nr = IO.trytee(rda, wrb, size, 0)
    assert_equal 5, nr
    assert_equal str, rdb.sysread(5)
    assert_equal str, rda.sysread(5)
  end

  def test_tee_eof
    rda, wra = IO.pipe
    rdb, wrb = IO.pipe
    wra.close
    assert_raises(EOFError) { IO.tee(rda.fileno, wrb.fileno, 4096, 0) }
  end

  def test_trytee_eof
    rda, wra = IO.pipe
    rdb, wrb = IO.pipe
    wra.close
    assert_nil IO.trytee(rda, wrb, 4096)
  end

  def test_tee_nonblock
    rda, wra = IO.pipe
    rdb, wrb = IO.pipe
    assert_raises(Errno::EAGAIN) {
      IO.tee(rda.fileno, wrb.fileno, 4096, IO::Splice::F_NONBLOCK)
    }
  end

  def test_trytee_nonblock
    rda, wra = IO.pipe
    rdb, wrb = IO.pipe
    assert_equal :EAGAIN, IO.trytee(rda, wrb, 4096)
  end

  def test_tee_io
    str = 'abcde'
    size = 5
    rda, wra = IO.pipe
    rdb, wrb = IO.pipe

    assert_nothing_raised { wra.syswrite(str) }
    nr = IO.tee(rda, wrb, size, 0)
    assert_equal 5, nr
    assert_equal str, rdb.sysread(5)
    assert_equal str, rda.sysread(5)
  end

  def test_vmsplice_array
    data = %w(hello world how are you today)
    r, w = IO.pipe
    n = IO.vmsplice(w.fileno, data, 0)
    assert_equal data.join('').size, n
    assert_equal data.join(''), r.readpartial(16384)
  end

  def test_vmsplice_noflags
    data = %w(hello world how are you today)
    r, w = IO.pipe
    n = IO.vmsplice(w, data)
    assert_equal data.join('').size, n
    assert_equal data.join(''), r.readpartial(16384)
  end

  def test_vmsplice_string
    r, w = IO.pipe
    assert_equal 5, IO.vmsplice(w, 'hello', 0)
    assert_equal 'hello', r.read(5)
  end

  def test_vmsplice_array_io
    data = %w(hello world how are you today)
    r, w = IO.pipe
    n = IO.vmsplice(w, data, 0)
    assert_equal data.join('').size, n
    assert_equal data.join(''), r.readpartial(16384)
  end

  def test_vmsplice_nonblock
    data = %w(hello world how are you today)
    r, w = IO.pipe
    w.syswrite('.' * IO::Splice::PIPE_CAPA)
    assert_raises(Errno::EAGAIN) {
      IO.vmsplice(w.fileno, data, IO::Splice::F_NONBLOCK)
    }
  end

  def test_vmsplice_in_full
    empty = ""

    # bs * count should be > PIPE_BUF
    [ [ 512, 512 ], [ 131073, 3 ], [ 4098, 64 ] ].each do |(bs,count)|
      rd, wr = IO.pipe
      buf = File.open('/dev/urandom', 'rb') { |fp| fp.sysread(bs) }

      vec = (1..count).map { buf }
      pid = fork do
        wr.close
        tmp = []
        begin
          sleep 0.005
          tmp << rd.readpartial(8192)
        rescue EOFError
          break
        end while true
        ok = (vec.join(empty) == tmp.join(empty))
        exit! ok
      end
      assert_nothing_raised { rd.close }
      assert_equal(bs * count, IO.vmsplice(wr.fileno, vec, 0))
      assert_nothing_raised { wr.close }
      _, status = Process.waitpid2(pid)
      assert status.success?
    end
  end

  def test_vmsplice_nil
    data = %w(hello world how are you today)
    assert_raises(TypeError) { IO.vmsplice(nil, data, 0) }
  end

  def test_constants
    assert IO::Splice::PIPE_BUF > 0
    %w(move nonblock more gift).each { |x|
      assert Integer === IO::Splice.const_get("F_#{x.upcase}")
    }
    assert IO::Splice::PIPE_CAPA >= IO::Splice::PIPE_BUF
  end

  def test_pipe_size
    r, w = IO.pipe
    assert_kind_of Integer, r.pipe_size
    assert(r.pipe_size >= 512)
    assert_nothing_raised { w.pipe_size = 8192 }
    assert_equal 8192, r.pipe_size

    w.write('*' * 4097)
    assert_raises(Errno::EBUSY) { r.pipe_size = 4096 }

    pipe_max_size = File.read("/proc/sys/fs/pipe-max-size").to_i
    assert_nothing_raised { r.pipe_size = pipe_max_size }
    assert_raises(Errno::EPERM) { r.pipe_size = pipe_max_size * 2 }
  end if IO.method_defined?(:pipe_size)
end
