# -*- encoding: binary -*-
require 'test/unit'
require 'tempfile'
$-w = true
require 'io/splice'
Thread.abort_on_exception = true

class Test_IO_Splice_In_Full < Test::Unit::TestCase
  def test_splice_in_full
    rd, wr = IO.pipe
    tmp = Tempfile.new 'splice-read'
    Thread.new do
      111.times do
        sleep 0.002
        wr.write "HIHIHI"
      end
    end
    nr = IO.splice rd, nil, tmp, nil, 666, IO::Splice::WAITALL
    assert_equal 666, nr
    tmp.rewind
    assert_equal "HIHIHI" * 111, tmp.read
  end

  def test_tee_in_full
    rd, wr = IO.pipe
    a, b = IO.pipe
    thr = Thread.new { a.read(666) }
    Thread.new do
      111.times do
        sleep 0.001
        wr.syswrite "HIHIHI"
      end
    end
    nr = IO.tee rd, b, 666, IO::Splice::WAITALL
    assert_equal 666, nr
    thr.join
    assert_equal "HIHIHI" * 111, thr.value
  end
end if defined?(RUBY_ENGINE)
