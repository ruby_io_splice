manifest = File.exist?('.manifest') ?
  IO.readlines('.manifest').map!(&:chomp!) : `git ls-files`.split("\n")

Gem::Specification.new do |s|
  s.name = %q{io_splice}
  s.version = (ENV["VERSION"] || '4.4.2').dup
  s.authors = ["Ruby io_splice hackers"]
  s.description = File.read('README').split("\n\n")[1]
  s.email = %q{ruby-io-splice@yhbt.net}
  s.extensions = %w(ext/io_splice/extconf.rb)
  s.extra_rdoc_files = IO.readlines('.document').map!(&:chomp!).keep_if do |f|
    File.exist?(f)
  end
  s.files = manifest
  s.homepage = 'https://yhbt.net/ruby_io_splice/'
  s.summary = 'zero-copy pipe I/O for Linux and Ruby (deprecated)'
  s.test_files = Dir['test/test_*.rb']

  s.licenses = %w(LGPL) # accessor not compatible with older RubyGems
end
