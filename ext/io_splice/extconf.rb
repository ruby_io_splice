require 'mkmf'
$CPPFLAGS << ' -D_GNU_SOURCE=1'

have_func('splice', %w(fcntl.h)) or abort 'splice(2) not defined'
have_func('tee', %w(fcntl.h)) or abort 'tee(2) not defined'
have_func('pipe2', %w(fcntl.h unistd.h))
have_func('rb_thread_blocking_region')
have_func('rb_thread_call_without_gvl')
have_macro('F_GETPIPE_SZ', %w(fcntl.h))
have_macro('F_SETPIPE_SZ', %w(fcntl.h))

create_makefile('io_splice_ext')
